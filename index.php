


<html lang >

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>This is the title. It is displayed in the titlebar of the window in most browsers.</title>
    <meta name="description" content="Here is a short description for the page. This text is displayed e. g. in search engine result listings.">
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/add.css" rel="stylesheet"> <!-- You could add your own css-definitions in a file - but you have to create it first... -->
    <!-- The following css-definitions are used just for showing you where the components of this page are placed. Feel free to delete the whole style-tag and to remove the classes in the html elements. -->
    <style>
        .bg-color01 {
            background-color: #AAA;
        }
        .bg-color02 {
            background-color: #9BC;
        }
        .bg-color03 {
            background-color: #579;
        }
        div[class*='level'] {
            border: 1px solid black;
            margin: 2px;
        }
    </style>
    <!--[if lt IE 9]>
      <script src="./js/html5shiv.min.js"></script>
      <script src="./js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <header class="container">
        <p>The header is usually the place for a logo or a picture, a navigation bar or a search field.</p>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Solve IT yourself!</a>
                </div>

                
            </div><!-- /.container-fluid -->
        </nav>
    </header>
	
    <section class="container bg-color01">
        <h1>Hallo IT 20a</h1>
        <p>Hier können sie ihren BMI Berechnen</p>
        <p class="jumbotron">BMI Rechner <span class="glyphicon glyphicon-thumbs-up"></span></p>
        <article id="tree"> <!-- this article is filled with example-data -->
            <div class="row level1">
                <div id="content" class="col-md-12">
                    <h2>Alter und Geschlecht</h2>
                    <p>Alter angeben</p>
					<input type="number" name="Alter" placeholder="Alter in Jahren" /> 
					<p>Wähle dein Geschlecht aus</p>
					<input type="radio" name="Geschlecht" value="Männlich" /> Männlich
					<input type="radio" name="Geschlecht" value="Weiblich" /> Weiblich 
                </div>
                
            </div>
            <div class="row level2">
               
                    <h2>Größe und Gewicht</h2>
					<p>Größe in cm angeben</p>
					<input type="number" name="Koerpergroeße" placeholder="Größe in cm" /> 
                    <p>Gewicht in ganzen Kilogramm angeben</p>
					<input type="number" name="Gewicht" placeholder="Gewicht in Kg" /> 
					<input type="submit" value="BMI berechnen">
						<?php

						if (isset($_POST["Koerpergroeße"]) && isset($_POST["Gewicht"])) 
						{
							$groeße = $_POST["Koerpergroeße"];
							$gewicht = $_POST["Gewicht"];
							$ergebnis = $gewicht/(($groeße/100)*($groeße/100));
						};
						?>
					<b> Dein BMI ist: </b>
						<?php
						if (isset($ergebnis)) 
						{
							echo round($ergebnis,2);
						};
						?>
			</div>
               
            
        </article>
        
    </section>
    <footer class="container bg-color02">
        <p>Das hier ist der BMI Rechner von Björn Martensen </p>
    </footer>
    
</body>
</html>
